﻿using System;
using System.IO;
class Program
{
    static void Main(string[] args)
    {
        if(!Options.parse(args)){
            Environment.Exit(1);
        }
        StorageClient downloadClient = new StorageClient(Options.downloadLink);
        StorageClient uploadClient = new StorageClient(Options.uploadLink);
        var files = downloadClient.downloadFiltered(Options.tempDir, FileNameAnalyzer.getNewestFileNames)
            .GetAwaiter()
            .GetResult();
        files.ForEach((file) =>
        {
            string jsonString = File.ReadAllText(file);
            jsonString = JsonAnalyzer.analyze(jsonString);
            File.WriteAllText(file, jsonString);
            uploadClient.upload(file, FileNameAnalyzer.getUploadFileName(file)).GetAwaiter().GetResult();
            File.Delete(file);
        });
    }
}
