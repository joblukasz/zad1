using System.Collections.Generic;
using System;
class FileNameAnalyzer
{
    public static List<string> getNewestFileNames(List<string> fileNames)
    {
        Dictionary<string, double> fileNamesCondidate = new Dictionary<string, double>();

        fileNames.ForEach((fileName) =>
        {
            var fileNameParts = getFileNamesParts(fileName);
            string namePrefix = String.Format("{0}_{1}", fileNameParts[0], fileNameParts[1]);
            double currentValue = 0;
            double valueInDict = 0;
            double.TryParse(fileNameParts[2], out currentValue);
            fileNamesCondidate.TryGetValue(namePrefix, out valueInDict);
            fileNamesCondidate[namePrefix] = currentValue > valueInDict ? currentValue : valueInDict;
        });
        List<string> newestFileNames = new List<string>();

        foreach (KeyValuePair<string, double> entry in fileNamesCondidate)
        {
            newestFileNames.Add(String.Format("{0}_{1}.json", entry.Key, entry.Value));
        };
        return newestFileNames;
    }
    public static string getUploadFileName(string path)
    {
        var pathParts = path.Split("/");
        string fileName = pathParts[pathParts.Length - 1];
        var fileNameParts = getFileNamesParts(fileName);
        fileName = String.Format("{0}_{1}_snapshot.json", fileNameParts[0], fileNameParts[1]);
        return fileName;
    }

    private static string[] getFileNamesParts(string fileName)
    {
        return fileName.Split(".")[0].Split("_");

    }
}