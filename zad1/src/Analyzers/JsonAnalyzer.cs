using Newtonsoft.Json.Linq;
using System.Linq;

class JsonAnalyzer
{
    public static string analyze(string jsonString)
    {
        JObject json = JObject.Parse(jsonString);
        parseGeneralStatus(json);
        parseMetrics(json);
        return json.ToString();
    }
    private static JObject parseGeneralStatus(JObject json)
    {
        json["General"]["GeneralStatus"] = int.Parse((string)json["General"]["GeneralStatus"]) % 2 == 1 ? "RUN" : "STOP";
        return json;
    }

    private static JObject parseMetrics(JObject json)
    {
        var condidatesToRemove = json.SelectTokens("..q")
            .Where((s) => int.Parse((string)s) < 192)
            .ToArray();
        foreach (var temp in condidatesToRemove)
        {
            temp.Parent.Parent.Parent.Remove();
        }
        return json;
    }
}

