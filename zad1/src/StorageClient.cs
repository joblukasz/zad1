using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;

class StorageClient
{
    CloudBlobContainer cloudBlobContainer = null;

    public StorageClient(string connectionString)
    {
        cloudBlobContainer = new CloudBlobContainer(new Uri(connectionString));
    }

    public async Task<List<string>> getBlobsNames()
    {
        BlobContinuationToken blobContinuationToken = null;
        List<string> blobs = new List<string>();
        do
        {
            var results = await cloudBlobContainer.ListBlobsSegmentedAsync(null, blobContinuationToken);
            blobContinuationToken = results.ContinuationToken;
            foreach (IListBlobItem item in results.Results)
            {
                blobs.Add((item as CloudBlob).Name);
            }
        } while (blobContinuationToken != null);
        return blobs;
    }

    public async Task<string> download(string fileName, string destionLocation)
    {
        createDestDirectory(destionLocation);
        CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);
        await cloudBlockBlob.DownloadToFileAsync(getFilePath(fileName, destionLocation), FileMode.Create);
        return fileName;
    }

    public async Task<List<string>> downloadFiltered(string destionLocation, Func<List<string>, List<string>> filterMethod)
    {
        List<Task> tasks = new List<Task>();
        List<string> downloadedFiles = new List<string>();
        var fileNames = await getBlobsNames();
        fileNames = filterMethod(fileNames);
        fileNames.ForEach((fileName) =>
        {
            tasks.Add(download(fileName, destionLocation));
            downloadedFiles.Add(getFilePath(fileName, destionLocation));
        });
        Task.WaitAll(tasks.ToArray());
        return downloadedFiles;
    }

    public async Task upload(string fileName, string destFileName)
    {
        CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(destFileName);
        await cloudBlockBlob.UploadFromFileAsync(fileName);
    }
    
    private void createDestDirectory(string destionLocation)
    {
        if (!Directory.Exists(destionLocation))
        {
            DirectoryInfo di = Directory.CreateDirectory(destionLocation);
        }
    }
    private static string getFilePath(string fileName, string destionLocation)
    {
        return String.Format("{0}/{1}", destionLocation, fileName);
    }
}
