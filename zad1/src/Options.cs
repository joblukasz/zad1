using System;
using System.Collections.Generic;
using Mono.Options;
class Options
{
    static public string downloadLink;
    static public string uploadLink;
    static public string tempDir = ".";
    static private bool show_help;
    static public List<string> extra;
    static OptionSet p = new OptionSet() {
            { "dl|download=", "URL from were files should be downloaded", v => downloadLink = v},
            { "ul|upload=", "URL were files should be uploaded", v => uploadLink = v },
            { "tmp|temporary=", "Temporaty directory location", v => tempDir = v },
            { "h|help",  "show this message and exit", v => show_help = v != null },
        };
    public static bool parse(string[] args)
    {
        try
        {
            extra = p.Parse(args);
        }
        catch (OptionException e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine("Try `zad1 --help' for more information.");
            return false;
        }

        if (show_help)
        {
            showHelp();
            return false;
        }
        return true;
    }

    private static void showHelp()
    {

        Console.WriteLine("Usage: zad1 [OPTIONS]+ message");
        Console.WriteLine("Options:");
        p.WriteOptionDescriptions(Console.Out);
    }

}